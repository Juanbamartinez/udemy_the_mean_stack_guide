import {Component} from '@angular/core';
// Se inyecta acá para que todos los componentes que están dentro del appComponent, usen la misma instancia del Service
import {MessageService} from "./messages/message.service";
@Component({
    selector: 'my-app',
    templateUrl: './app.component.html'
})
export class AppComponent {
}