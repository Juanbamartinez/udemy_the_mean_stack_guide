export class User {
    /**
        user public delante del parámetro es lo mismo que:
        email: string;
        constructor(email: string){
            this.email = email;
        }
    **/

    // public firsName?: string --> hace esas propiedades opcionales
    constructor(public email: string,
                public password: string,
                public firstName?: string,
                public lastName?: string){

    }
}