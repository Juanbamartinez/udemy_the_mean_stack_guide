import {Routes, RouterModule} from "@angular/router";
import {MessagesComponent} from "./messages/messages.component";
import {AuthenticationComponent} from "./auth/authentication.component";
//import {AUTH_ROUTES} from "./auth/auth.routing";

const APP_ROUTES: Routes = [
    // pathMatch: "full" soluciona el problema de path: "" para el redirectTo
    {path: '', redirectTo: '/messages', pathMatch: 'full' },
    {path: 'messages', component: MessagesComponent },
    // children, toma rutas para registrar las rutas como childroutes del path al cual se le hace append de children
    //{path: 'auth', component: AuthenticationComponent, children: AUTH_ROUTES }
    {path: 'auth', component: AuthenticationComponent, loadChildren: './auth/auth.module#AuthModule' } // lazy-loading
];

// Registra nuestras rutas en el módulo de Router
// Se hace un export para poder importar las rutas en el app.component
export const routing = RouterModule.forRoot(APP_ROUTES);