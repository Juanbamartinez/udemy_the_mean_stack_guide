import {Component, Input, Output, EventEmitter, OnInit} from '@angular/core';
import {NgForm} from '@angular/forms';
import {MessageService} from "./message.service";
import {Message} from "./message.model";

@Component({
    selector: 'app-message-input',
    templateUrl: './message-input.component.html',
    styleUrls: ['./message.component.css']
})
export class MessageInputComponent implements OnInit {
    message: Message;

    constructor(private messageService: MessageService){}

    ngOnInit(){
        this.messageService.messageIsEdited.subscribe(
            (message: Message) => this.message = message
        );
    }

    onSubmit(form: NgForm){
        if (this.message){
            this.message.content = form.value.content;
            this.messageService.updateMessage(this.message)
                .subscribe(
                    result => console.log(result)
                );
            this.message = null;
        } else {
            const  m = new Message(form.value.content, 'Juanba');
            this.messageService.addMessage(m)
                .subscribe(
                    data => console.log("Nuevo message: ",data),
                    error => console.log("Error: ",error)
                );
        }
        form.resetForm();
    }

    onClear(form: NgForm){
        this.message = null;
        form.resetForm();
    }
}