import {Component, Input, Output, EventEmitter, OnInit} from '@angular/core';
import {MessageService} from "./message.service";
import {Message} from "./message.model";

@Component({
    selector: 'app-message-list',
    templateUrl: './message-list.component.html',
    styleUrls: ['./message.component.css']
})
export class MessageListComponent implements OnInit {
    messages: Message[];

    constructor(private messageService: MessageService) {
    }

    ngOnInit() {
        this.messageService.getMessages()
            .subscribe(
                (messages: Message[]) => {
                    this.messages = messages
                    console.log(this.messages);
                }
            );
    }
}