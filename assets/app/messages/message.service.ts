import {Http, Response, Headers} from "@angular/http";
import {Injectable, EventEmitter} from "@angular/core";
import 'rxjs/Rx';
import {Observable} from "rxjs";
import {Message} from "./message.model";
import {ErrorService} from "../errors/error.service";

// Agrega metadata
@Injectable()
export class MessageService {
    constructor(private http: Http, private errorService: ErrorService){};

    private messages: Message[] = [];
    messageIsEdited = new EventEmitter<Message>();

    // local http://localhost:3000/

    addMessage(m: Message){
        const body = JSON.stringify(m);
        const headers = new Headers({
            'Content-Type': 'application/json'
        });
        // Set-up un Observable (mantiene el request), no hace el request hasta que alguien se subscriba
        // map() transforma data que es devuelta desde el servidor
        const token = localStorage.getItem('token') ? '?token='+localStorage.getItem('token') : '';
        // se envía el token en cada request
        return this.http.post('http://localhost:3000/message' + token, body, {headers: headers})
            .map((response: Response) => {
                const result = response.json();
                const message = new Message(result.obj.content, result.obj.user.firstName, result.obj._id, result.obj.user._id);
                this.messages.push(message);
                return message;
            })
            .catch((error: Response) => {
                this.errorService.handleError(error.json());
                return Observable.throw(error.json());
            });
    }

    getMessages(){
        return this.http.get('http://localhost:3000/message')
            .map((response: Response) => {
                const messages = response.json().obj;
                let transformedMessages: Message[] = [];
                for (let m of messages){
                    transformedMessages.push(new Message(m.content, m.user.firstName, m._id, m.user._id));
                }
                this.messages = transformedMessages;
                return transformedMessages;
            })
            .catch((error: Response) => {
                this.errorService.handleError(error.json());
                return Observable.throw(error.json());
            });
    }

    editMessage(m: Message){
        this.messageIsEdited.emit(m);
    }

    updateMessage(m: Message){
        const body = JSON.stringify(m);
        const headers = new Headers({
            'Content-Type': 'application/json'
        });
        const token = localStorage.getItem('token') ? '?token='+localStorage.getItem('token') : '';
        // se envía el token en cada request
        return this.http.patch('http://localhost:3000/message/' + m.messageId + token, body, {headers: headers})
            .map((response: Response) => response.json())
            .catch((error: Response) => {
                this.errorService.handleError(error.json());
                return Observable.throw(error.json());
            });
    }

    deleteMessage(m: Message){
        this.messages.splice(this.messages.indexOf(m), 1);
        const token = localStorage.getItem('token') ? '?token='+localStorage.getItem('token') : '';
        // se envía el token en cada request
        return this.http.delete('http://localhost:3000/message/' + m.messageId + token)
            .map((response: Response) => response.json())
            .catch((error: Response) => {
                this.errorService.handleError(error.json());
                return Observable.throw(error.json());
            });
    }
}