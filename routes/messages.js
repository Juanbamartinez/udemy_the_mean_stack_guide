var express = require('express');
var router = express.Router();
var jwt = require('jsonwebtoken');
var User = require('../models/user');
var Message = require('../models/message');

router.get('/', function(req, res, next){
   Message.find()
       // populate() expande la data que se retorna
       .populate('user', 'firstName')
       // exec() concatena funciones
       .exec(function(err, messages){
          if (err){
              return res.status(500).json({
                  title: 'An error ocurred',
                  error: err
              });
          }
          res.status(200).json({
              message: 'Success',
              obj: messages
          });
       });
});

// con use() se alcanza cada request que se haga
router.use('/', function(req, res, next){
    jwt.verify(req.query.token, 'secret', function(err, decoded){
        if (err){
            // 401 --> No autorizado
            return res.status(401).json({
                title: 'Not authenticated',
                error: err
            });
        }
        next();
    });
});

router.post('/', function(req, res, next){
    // decode() no valida el token
    var decoded = jwt.decode(req.query.token);
    User.findById(decoded.user._id, function(err, user){
        if (err){
            return res.status(500).json({
                title: 'An error ocurred',
                error: err
            });
        }
        var message = new Message({
            content: req.body.content,
            user: user._id
        });
        message.save(function(err, result){
            if (err){
                return res.status(500).json({
                    title: 'An error ocurred',
                    error: err
                });
            }
            // Se pushea el nuevo mensaje al array de mensajes del usuario
            user.messages.push(result);
            // Se guarda el estado del usuario actualizado
            user.save();
            result.user = user;
            // No se utiliza return porque se ejecuta si err es null
            // 201 --> se recibió la data con éxito
            res.status(201).json({
                message: 'Saved message',
                obj: result
            });
        });
    });
});

// patch() se utiliza para modificar data existente
router.patch('/:id', function(req, res, next){
    var decoded = jwt.decode(req.query.token);
    Message.findById(req.params.id, function(err, message){
        if (err){
            return res.status(500).json({
                title: 'An error ocurred',
                error: err
            });
        }
        if (!message){
            return res.status(500).json({
                title: 'No message found!',
                error: {
                    message: 'Message nor found'
                }
            });
        }
        if (message.user != decoded.user._id){
            return res.status(401).json({
                title: 'Not authenticated',
                error: err
            });
        }
        message.content = req.body.content;
        message.save(function(err, result){
            if (err){
                return res.status(500).json({
                    title: 'An error ocurred',
                    error: err
                });
            }
            // No se utiliza return porque se ejecuta si err es null
            // 201 --> se recibió la data con éxito
            res.status(200).json({
                message: 'Updated message',
                obj: result
            });
        });
    });
});

router.delete('/:id', function(req, res, next){
    var decoded = jwt.decode(req.query.token);
    Message.findById(req.params.id, function(err, message){
        if (err){
            return res.status(500).json({
                title: 'An error ocurred',
                error: err
            });
        }
        if (!message){
            return res.status(500).json({
                title: 'No message found!',
                error: {
                    message: 'Message nor found'
                }
            });
        }
        if (message.user != decoded.user._id){
            return res.status(401).json({
                title: 'Not authenticated',
                error: {
                    message: 'Users do not match'
                }
            });
        }
        message.remove(function(err, result){
            if (err){
                return res.status(500).json({
                    title: 'An error ocurred',
                    error: err
                });
            }
            // No se utiliza return porque se ejecuta si err es null
            // 201 --> se recibió la data con éxito
            res.status(201).json({
                message: 'Deleted message',
                obj: result
            });
        });
    });
});

module.exports = router;
