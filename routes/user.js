var express = require('express');
var router = express.Router();
var bcrypt = require('bcryptjs');
var jwt = require('jsonwebtoken');
var User = require('../models/user');

router.post('/', function(req, res, next){
    var user = new User({
        firstName: req.body.firstName,
        lastName: req.body.lastName,
        password: bcrypt.hashSync(req.body.password, 10),
        email: req.body.email
    });
    user.save(function(err, result){
        if (err){
            return res.status(500).json({
                title: 'An error ocurred',
                error: err
            });
        }
        res.status(201).json({
            message: 'User created',
            obj: result
        });
    });
});

router.post('/signin', function(req, res, next){
    User.findOne({
        // criterio por el cual buscar con el findOne()
        email: req.body.email
    }, function(err, user){
        if (err){
            return res.status(500).json({
                title: 'An error ocurred',
                error: err
            });
        }
        if (!user){
            return res.status(401).json({
                title: 'Login failed',
                error: {
                    message: 'Invalid login credentials'
                }
            });
        }
        if (!bcrypt.compareSync(req.body.password, user.password)){
            return res.status(401).json({
                title: 'Login failed',
                error: {
                    message: 'Invalid login credentials'
                }
            });
        }
        var token = jwt.sign(
            {user: user},       //payload
            'secret',           // secret or private key
            {expiresIn: 7200}   // options
                                // callback
        );
        res.status(200).json({
           message: 'Successfully logged in',
            token: token,
            userId: user._id
        });
    });
});

module.exports = router;