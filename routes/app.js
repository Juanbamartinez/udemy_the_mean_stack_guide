var express = require('express');
var router = express.Router();

router.get('/', function(req, res, next){
   res.render('index');
});

module.exports = router;

// var User = require('../models/user');
// router.get('/', function (req, res, next) {
//     User.findOne({}, function(err, doc){
//         if (err){
//             return res.send('Error!');
//         }
//         // En caso de no haber error, se hace el render dentro del callback de la función que hace el fetch
//         // para poder tener acceso a esos datos ya que es una función asíncrona
//         res.render('node', {email: doc.email});
//     });
// });
// router.get('/message', function(req, res, next){
//     res.render('node', {message: 'Hola'});
// });
// router.post('/', function(req, res, next){
//     var email = req.body.email;
//     var user = new User({
//         firstName: 'Juan',
//         lastName: 'Martinez',
//         password: 'user123',
//         email: email
//     });
//     // le dice a mongoose que guarde el objeto en la colección correspondiente
//     user.save();
//     res.redirect('/');
// });
